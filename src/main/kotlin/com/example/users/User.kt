package com.example.users

data class User(
        val id: Long,
        var firstName: String,
        var lastName: String,
        var email: String
)
