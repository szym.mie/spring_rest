package com.example.users

import org.springframework.web.bind.annotation.*

@RestController
class UsersController {
    private val users: Users = Users()

    @GetMapping("/users")
    fun findUser(@RequestParam(required = false) id: Long?): Set<User>? =
        try {
            val user = users.find(id!!)
            if (user != null) setOf(user)
            else null
        } catch (e: NullPointerException) {
            users.all()
        }

    @PostMapping("/users")
    fun newUser(@RequestParam(required = false) firstName: String?,
                @RequestParam(required = false) lastName: String?,
                @RequestParam(required = false) email: String?) {
        users.new(firstName ?: "", lastName ?: "", email ?: "")
    }

    @PutMapping("/users")
    fun editUser(@RequestParam id: Long,
                 @RequestParam(required = false) firstName: String?,
                 @RequestParam(required = false) lastName: String?,
                 @RequestParam(required = false) email: String?) {
        users.edit(id, firstName, lastName, email)
    }

    @DeleteMapping("/users")
    fun deleteUser(@RequestParam id: Long) {
        users.delete(id)
    }
}