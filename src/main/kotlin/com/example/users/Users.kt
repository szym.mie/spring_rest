package com.example.users

class Users {
    private val users: MutableSet<User> = mutableSetOf()
    private val empty: MutableSet<Long> = mutableSetOf()
    private var gid: Long = 0L

    private val predicate: (Long, User) -> Boolean = { id, user -> user.id == id }

    fun new(firstName: String="John",
            lastName: String="Doe",
            email: String="john.doe@gmail.com") {
        val new = User(
            try { empty.elementAt(0) }
            catch (e: IndexOutOfBoundsException) { gid++ },
            firstName,
            lastName,
            email)
        users.add(new)
        try { empty.remove(empty.elementAt(0)) }
        catch (e: IndexOutOfBoundsException) {}
    }

    fun find(id: Long): User? = try { users.first { predicate(id, it) } } catch (e: NoSuchElementException) { null }

    fun all(): Set<User> = users.toSet()

    fun edit(id: Long,
             firstName: String?=null,
             lastName: String?=null,
             email: String?=null) {
        try {
            val user = users.first { predicate(id, it) }
            user.firstName = firstName ?: user.firstName
            user.lastName = lastName ?: user.lastName
            user.email = email ?: user.email
        } catch (e: NoSuchElementException) {}
    }

    fun delete(id: Long) {
        try {
            users.remove( users.first { predicate(id, it) } )
            empty.add(id)
        } catch (e: NoSuchElementException) {}
    }
}