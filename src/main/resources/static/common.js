const view = {
    root: document.getElementById("view"),
    header_names: ["ID", "First", "Last", "Email"],
    data: function (data) {
        this.root.innerHTML = "";

        const header = document.createElement("tr");
        for (const name of this.header_names) {
            const element = document.createElement("th");
            element.innerText = name;
            header.appendChild(element);
        }

        this.root.appendChild(header);

        for (const atom of data) {
            const row = document.createElement("tr");
            const elements = Object.values(atom).map(value => {
                const column = document.createElement("td");
                column.innerText = value;
                return column;
            });
            for (const element of elements) row.appendChild(element);
            this.root.appendChild(row);
        }
    }
};

const operations = {
    choice: null,
    new: function () {
        dialog.hide();
        dialog.show_new();
        this.choice = 0
    },
    edit: function () {
        dialog.hide();
        dialog.show_id();
        dialog.show_new();
        this.choice = 1;
    },
    find: function () {
        dialog.hide();
        dialog.show_id();
        this.choice = 2;
    },
    delete: function () {
        dialog.hide();
        dialog.show_id();
        this.choice = 3;
    },
    reload: function (id) {
        fetch(id === undefined ? "/users" : `/users?id=${id}`)
            .then(res => res.json())
            .then(json => { view.data(json); })
            .catch(err => { console.log(err); });
    },
    commit: function () {
        const user = dialog.result_new();
        const id = dialog.result_id();
        switch (this.choice) {
            case 0:
                fetch(`/users?firstName=${user.first}&lastName=${user.last}&email=${user.email}`, {method: "post"})
                    .then(_ => { this.reload(); })
                    .catch(err => { console.error(err); });
                break;
            case 1:
                fetch(`/users?id=${id}&firstName=${user.first}&lastName=${user.last}&email=${user.email}`, {method: "put"})
                    .then(_ => { this.reload(); })
                    .catch(err => { console.error(err); });
                break;
            case 2:
                this.reload(id);
                break;
            case 3:
                fetch(`/users?id=${id}`, {method: "delete"})
                    .then(_ => { this.reload(); })
                    .catch(err => { console.error(err); });
                break;
            default:
                break;
        }
    }
};

const dialog = {
    dialogs: {
        id: document.getElementById("id"),
        new: document.getElementById("new")
    },
    inputs: {
        id: document.getElementById("id_input"),
        first: document.getElementById("first_input"),
        last: document.getElementById("last_input"),
        email: document.getElementById("email_input"),
        commit: document.getElementById("commit")
    },
    show_id: function () {
        this.inputs.commit.style.display = "block";
        this.dialogs.id.style.display = "block"
    },
    show_new: function () {
        this.inputs.commit.style.display = "block";
        this.dialogs.new.style.display = "block";
    },
    hide: function () {
        this.inputs.commit.style.display = "none";
        this.dialogs.id.style.display = "none";
        this.dialogs.new.style.display = "none";
    },
    result_id: function () {
        return this.inputs.id.value;
    },
    result_new: function () {
        return {
            first: this.inputs.first.value,
            last: this.inputs.last.value,
            email: this.inputs.email.value
        };
    }
}

dialog.hide();